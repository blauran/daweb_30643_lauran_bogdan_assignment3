import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';
import { HttpClient } from '@angular/common/http';
import { map, first } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  user$: Observable<User[]>;
  logged: User;
  
  constructor(private http: HttpClient, private auth: AuthService, private router: Router, private _sanitizer: DomSanitizer) { }


  ngOnInit() {
    this.logged = JSON.parse(sessionStorage.getItem('user')) as User;
    this.user$ = this.http.get<User[]>("http://127.0.0.1:8000/api/profile").pipe(
      map(users=>users.filter(user=>user.email === this.logged.email))
    );

    console.log(this.logged);
  }

  onSubmit(){
      this.router.navigate(['edit']);
  }

}
