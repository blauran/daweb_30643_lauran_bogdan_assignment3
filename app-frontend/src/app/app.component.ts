import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './models/user.model';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'daw1';
  constructor(private auth: AuthService) { }

  ngOnInit(){

    const user = sessionStorage.getItem('user');
    if(!user){return;}

    this.auth.loggedStatus.next(JSON.parse(user));
  }

}
