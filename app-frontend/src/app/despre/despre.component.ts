import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { User } from '../models/user.model';

@Component({
  selector: 'app-despre',
  templateUrl: './despre.component.html',
  styleUrls: ['./despre.component.css']
})
export class DespreComponent implements OnInit {

  comms: Observable<Comment[]>;
  logged: User;

  constructor(private http:HttpClient, private auth:AuthService) { }

  ngOnInit() {
    this.logged = JSON.parse(sessionStorage.getItem('user')) as User;
    this.comms = this.http.get<Comment[]>("http://127.0.0.1:8000/api/comments");
  }

  onSubmit(email: string, content: string){
    email = this.logged.email;
    this.http.post<Comment>("http://127.0.0.1:8000/api/addComment", {email, content}).toPromise().then(() => alert('Comentariu adaugat cu success')).catch(() => alert('Nu s-a putut adauga comentariul'))
  }

}
