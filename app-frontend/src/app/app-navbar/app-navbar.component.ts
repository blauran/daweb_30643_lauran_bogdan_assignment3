import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { User } from '../models/user.model';
import { AuthService } from '../services/auth.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-navbar',
  template: `
  <div class="page-header" style="background-color:rgb(220, 220, 220);">
        <div class="row flex-nowrap justify-content-between align-items-center">
          <div class="col-4 text-center">
          <button (click)="onChangeLanguage()" class="btn btn-outline-secondary">{{ 'changeLanguage' | translate }}</button>
          </div>
      <ul class="navbar-nav nav-right">
      <li *ngIf="!logged" class="nav-item">
        <a class="nav-link" [routerLink]="['/login']">Login</a>
      </li>
      <li *ngIf="!logged" class="nav-item">
        <a class="nav-link" [routerLink]="['/register']">Register</a>
      </li>

      <li *ngIf="logged" class="nav-item">
        <a class="nav-link" [routerLink]="['']" (click)="logout()">Logout</a>
      </li>

      <li *ngIf="logged" class="nav-item">
       <strong class="nav-link">{{user.name}}</strong>
      </li>
    </ul>
          </div>
          
  </div>
  <nav class="navbar sticky-top navbar-expand navbar-dark bg-dark">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
    
      <li class="nav-item">
        <a class="nav-link" [routerLink]="['/home']" routerLinkActive="active" href="#"> {{ 'navbar.acasa' | translate }} <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" [routerLink]="['/news']" routerLinkActive="active" href="#">{{ 'navbar.noutati' | translate }}</a>
      </li>
      <li class="nav-item" *ngIf="logged">
        <a class="nav-link" [routerLink]="['/about']" routerLinkActive="active" href="#">{{ 'navbar.despre' | translate }}</a>
      </li>
      <li class="nav-item" *ngIf="!logged">
        <a class="nav-link" [routerLink]="['/login']" routerLinkActive="active" href="#">{{ 'navbar.despre' | translate }}</a>
      </li>
      <li class="nav-item" *ngIf="logged">
        <a class="nav-link" [routerLink]="['/coordonator']" routerLinkActive="active" href="#">{{ 'navbar.coordonator' | translate }}</a>
      </li>
      <li class="nav-item" *ngIf="!logged">
        <a class="nav-link" [routerLink]="['/login']" routerLinkActive="active" href="#">{{ 'navbar.coordonator' | translate }}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" [routerLink]="['/contact']" routerLinkActive="active" href="#">{{ 'navbar.contact' | translate }}</a>
      </li>
      <li class="nav-item" *ngIf="logged">
        <a class="nav-link" [routerLink]="['/profile']" routerLinkActive="active" href="#">{{ 'navbar.profil' | translate }}</a>
      </li>
      <li class="nav-item" *ngIf="!logged">
        <a class="nav-link" [routerLink]="['/login']" routerLinkActive="active" href="#">{{ 'navbar.profil' | translate }}</a>
      </li>
    </ul>
    
  </div>
</nav>

  `,
  styleUrls: ['./app-navbar.component.css']
})

export class AppNavbarComponent implements OnInit {
  logged = false;
  user : User;
  isEnglish = false;

  constructor(private service: TranslateService, public auth: AuthService, private http: HttpClient) {}

  onChangeLanguage() {
    if (this.isEnglish) {
      this.service.use('ro');
    } else {
      this.service.use('en');
    }

    this.isEnglish = !this.isEnglish;
  }

  ngOnInit() {
    this.auth.loggedStatus.subscribe(isLogged => {
      this.logged = !!isLogged;
      this.user = isLogged;
    });
  }

  logout() {
    this.http.post("http://127.0.0.1:8000/api/logout","log off");
    this.auth.loggedStatus.next(null);
    sessionStorage.clear();
  }

}
