import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user.model';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private http: HttpClient, private auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit(event, email: HTMLInputElement, password: HTMLInputElement) {
    event.preventDefault();
    console.log(email.value, password.value);

    this.http.post<User>("http://127.0.0.1:8000/api/login", {email:email.value, password:password.value})
      .subscribe(user => {
        sessionStorage.setItem('user', JSON.stringify(user));
        if(sessionStorage.getItem('user')==="Invalid Credentials") {this.auth.loggedStatus.next(null); alert('Bad credentials');}
        else this.auth.loggedStatus.next(user);
        console.log(sessionStorage.getItem('user'));
        this.router.navigate(['']);
        
      }, err => alert('Bad credentials'));
  }


}
