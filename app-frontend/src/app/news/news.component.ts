import { Component, OnInit } from '@angular/core';
import { Parser } from 'xml2js';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  public xmlItems: any;
  
  constructor(private _http: HttpClient) { this.loadXML() }

  loadXML(){
      this._http.get('/assets/news.xml',
      {
        headers : new HttpHeaders()
        .set('Content-Type', 'text/xml')
        .append('Access-Control-Allow-Methods', 'GET')
        .append('Access-Control-Allow-Origin', '*')
        .append('Access-Control-Allow-Headers', "Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Request-Method"),
        responseType: 'text'
      })
      .subscribe((data) => {
        this.parseXML(data)
         .then((data) => {
           this.xmlItems = data;
          });
      });
  }

  parseXML(data){
    return new Promise(resolve =>{
      var k: string | number,
      arr = [],
      parser = new Parser({ trim: true, explicitArray: true });
      parser.parseString(data, function(err, result){
        var obj = result.News;
        for(k in obj.article){
          var item = obj.article[k];
          arr.push({
            title: item.title[0],
            link: item.link[0]
          });
        }
        resolve(arr);
      });
    });
  }
  

  ngOnInit() {
  }

}
