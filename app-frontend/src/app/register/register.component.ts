import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private http: HttpClient, private location: Location) { }

  ngOnInit() {
  }

  cancel(){
    this.location.back();
  }

  submit(email: string, password: string, name: string) {
    this.http.post('http://127.0.0.1:8000/api/signup', { name, email, password}).toPromise().then(() => alert('Utilizator creat cu succes')).catch(() => alert('Nu s-a putut crea utilizatorul'))
  }

}
