import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { User } from '../models/user.model';
import { Binary } from '@angular/compiler';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {

  logged: User;
  domenii: string[];
  domInteres: string[];

  constructor(private http: HttpClient, private router: Router, private auth:AuthService) {
    this.domenii = []; 
    this.domInteres=['Internet of Things', 'Cloud computing', 'Inteligență artificială și sisteme cognitive', 'High performance computing', 'Advanced materials']; 
  }

  ngOnInit() {
    this.logged = JSON.parse(sessionStorage.getItem('user')) as User;
  }

  onSelected(domeniu: string){
    this.domenii.push(domeniu);
  }

  onSubmit(email: string ,name: string, password: string, domenii_interes: string, picture: FileList){
    debugger
    email = this.logged.email;
    domenii_interes = this.domenii.join(", ");

    const formData = new FormData();
    formData.append('password', password);
    formData.append('email', email);
    formData.append('name', name);
    formData.append('domenii_interes', domenii_interes);
    formData.append('picture', picture[0], 'picture');

    this.http.post<User>("http://127.0.0.1:8000/api/update", formData).toPromise().then(() => alert('Utilizator modificat cu succes')).catch(() => alert('Nu s-a putut modifica utilizatorul'))
    this.router.navigate(['profile']);
  }

}
