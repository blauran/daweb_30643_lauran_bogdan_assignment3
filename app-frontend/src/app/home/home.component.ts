import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-home',
  template: `<app-navbar></app-navbar>
  <div class="carousel slide" id="slider" data-ride="carousel">
  <!--indicators-->
  <ol class="carousel-indicators">
    <li data-target="#slider" data-slide-to="0" class="active"></li>
    <li data-target="#slider" data-slide-to="1"></li>
    <li data-target="#slider" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="https://blog.datahut.co/wp-content/uploads/2019/07/web-crawler.png">
      <div class="carousel-caption">
        <h4>Welcome</h4>
      </div>
    </div>

    <div class="carousel-item" id="slide2">
      <img src="https://chopnews.com/wp-content/uploads/2019/06/Screen-Shot-2019-06-25-at-5.19.15-PM.png">
      <div class="carousel-caption">
        <h4>Welcome</h4>
      </div>
    </div>

    <div class="carousel-item" id="slide3">
      <img src="https://fiverr-res.cloudinary.com/images/t_main1,q_auto,f_auto/gigs/122515235/original/654cd9a2129bb5dbb359a2401b7cfc738e6b7b3c/do-web-scraping-and-data-mining.png">
      <div class="carousel-caption">
        <h4>Welcome</h4>
      </div>
    </div>

  </div>
  <a class="carousel-control-prev" href="#slider" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#slider" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
    <app-footer></app-footer>`,
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
