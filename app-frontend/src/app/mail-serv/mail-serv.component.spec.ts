import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailServComponent } from './mail-serv.component';

describe('MailServComponent', () => {
  let component: MailServComponent;
  let fixture: ComponentFixture<MailServComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailServComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailServComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
