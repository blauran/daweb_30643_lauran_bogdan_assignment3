import { Binary } from '@angular/compiler';
import { HexBase64BinaryEncoding } from 'crypto';

export interface User{
    id: number;
    name: string;
    email: string;
    picture: string;
    domenii_interes: string;
    password?: string;
    remember_token: string;
  }