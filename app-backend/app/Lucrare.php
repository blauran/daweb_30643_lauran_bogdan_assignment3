<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Lucrare extends Model
{
    protected $fillable = ['title', 'descriere', 'coordonator'];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
