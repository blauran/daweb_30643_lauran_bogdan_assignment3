<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(Guard $auth, Request $request){

        $fields = ['name', 'email', 'password'];
        $credentials = $request->only($fields);
        $validator = Validator::make(
            $credentials,
            [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
                'password' => 'required|min:6',
            ]
        );
        if($validator->fails()){
            return response($validator->errors());
        }

        $result = User::create([
            'name' => $credentials['name'],
            'email' => $credentials['email'],
            'password' => bcrypt($credentials['password']),
        ]);

        return $this->login($request);
    }

    public function login(Request $request) {

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $result['email'] = $credentials['email'];
            return response($result);
        }

        return response('Invalid Credentials');
    }

    public function logout(){
        Session::flush();
        Auth::logout();
        return response("logged out");
    }


}
