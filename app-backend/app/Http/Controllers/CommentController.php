<?php

namespace App\Http\Controllers;

use App\Comment;
use http\Client\Response;
use Illuminate\Http\Request;
use App\Lucrare;

class CommentController extends Controller
{
    public function index(){
        return Comment::all();
    }

    public function getById($id){
        return Comment::find($id);
    }

    public function addComment(Request $request){
        return Comment::create($request->all());
    }

    public function update(Request $request, $id){
        $lucrare = Comment::findOrFail($id);
        $lucrare->update($request->all());

        return $lucrare;
    }


}
