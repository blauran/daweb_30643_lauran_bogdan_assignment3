<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    public function index(){
        return User::all();
    }

    /*public function getByEmail(Request $request){
        $user = User::where('email','=', $request->header('email'))->first();
        return response()->json([$user]);

    }*/

    public function addUser(Request $request){
        return User::create($request->all());
    }

    public function update(Request $request){
        $email = $request['email'];
        $user = User::where('email', $email)->first();
        $user->name = $request['name'];
        $user->password = bcrypt($request['password']);
        $user->domenii_interes = $request['domenii_interes'];
        $user->picture = base64_encode(file_get_contents($request->file('picture')->getRealPath()));
        $user->save();

        return $user;
    }
}
