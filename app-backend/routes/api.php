<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('comments', 'CommentController@index');
Route::post('addComment','CommentController@addComment');

Route::get('profile','UserController@index');
Route::post('update', 'UserController@update');
Route::post('login', 'AuthController@login');
Route::post('signup', 'AuthController@register');
Route::any('logout','AuthController@logout');

